﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JsonClientTest
{
    internal class Program
    {
        private static void Main(string[] args)
        {
            JsonClient client = new JsonClient();
            client.Connect("localhost", 12345);
            Dictionary<string, List<float>> dict = new Dictionary<string, List<float>>();
            while (true)
            {
                if (false)
                //if (client.Connected == false) 
                {
                    Console.WriteLine("Disconnected!");
                    client.Dispose();
                    break;
                }
                bool receivedData = client.ReceiveJsonTransforms(out dict);
                if(receivedData) Console.WriteLine($"Received valid json: {dict.Count()}");
            }
        }
    }
}