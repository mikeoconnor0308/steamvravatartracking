﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AvatarRenderer : MonoBehaviour
{
    public class TransformLine
    {
        public Transform A;
        public Transform B;

        public TransformLine(Transform a, Transform b)
        {
            A = a;
            B = b;
        }
    }

    public Transform HeadModel;
    public Transform LeftHandModel;
    public Transform RightHandModel;
    public Transform LeftElbowModel;
    public Transform RightElbowModel;
    public Transform FrontModel;
    public Transform BackModel;
    public Transform LeftKneeModel;
    public Transform RightKneeModel;

    public LineRenderer BodyLinesPrefab;

    private Dictionary<TransformLine, LineRenderer> bodyLines = new Dictionary<TransformLine, LineRenderer>();

    /// <summary>
    /// Whether to draw lines.
    /// </summary>
    public bool DrawLines = true;

    private bool linesActive = false;
    // Use this for initialization
    private void Start()
    {
        GenerateLines();
    }

    private void GenerateLines()
    {
        ClearLines();
        BodyLinesPrefab.gameObject.SetActive(true);
        //Generate the lines we need for the body, this has to be hardcoded.
        TransformLine HeadToLeftElbow = new TransformLine(HeadModel, LeftElbowModel);
        TransformLine HeadToRightElbow = new TransformLine(HeadModel, RightElbowModel);
        TransformLine LeftElbowToLeftHand = new TransformLine(LeftElbowModel, LeftHandModel);
        TransformLine RightElbowToRightHand = new TransformLine(RightElbowModel, RightHandModel);
        TransformLine HeadToBack = new TransformLine(HeadModel, BackModel);
        TransformLine BackToFront = new TransformLine(BackModel, FrontModel);
        TransformLine FrontToLeftKnee = new TransformLine(FrontModel, LeftKneeModel);
        TransformLine FrontToRightKnee = new TransformLine(FrontModel, RightKneeModel);
        bodyLines.Add(HeadToLeftElbow, Instantiate(BodyLinesPrefab, transform));
        bodyLines.Add(HeadToRightElbow, Instantiate(BodyLinesPrefab, transform));
        bodyLines.Add(LeftElbowToLeftHand, Instantiate(BodyLinesPrefab, transform));
        bodyLines.Add(RightElbowToRightHand, Instantiate(BodyLinesPrefab, transform));
        bodyLines.Add(HeadToBack, Instantiate(BodyLinesPrefab, transform));
        bodyLines.Add(BackToFront, Instantiate(BodyLinesPrefab, transform));
        bodyLines.Add(FrontToLeftKnee, Instantiate(BodyLinesPrefab, transform));
        bodyLines.Add(FrontToRightKnee, Instantiate(BodyLinesPrefab, transform));
        BodyLinesPrefab.gameObject.SetActive(false);
        linesActive = true;
    }

    private void ClearLines()
    {
        foreach (var line in bodyLines.Values)
        {
            Destroy(line.gameObject);
        }
        bodyLines.Clear();
        linesActive = false;
    }

    // Update is called once per frame
    private void Update()
    {
        if(DrawLines)
        {
            if (linesActive == false)
                GenerateLines();
            UpdateLines();
        }
        else
        {
            if (linesActive == true)
                ClearLines();
        }
        
    }

    private void UpdateLines()
    {
        foreach (var keyVal in bodyLines)
        {
            var posFrom = keyVal.Key.A.position;
            var posTo = keyVal.Key.B.position;
            var line = keyVal.Value;
            line.SetPositions(new Vector3[] { posFrom, posTo });
        }
    }
}