﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Handles switching between the various UI elements in the scene.
/// </summary>
public class UIController : MonoBehaviour {

    private ConnectionController connectionController;
    [SerializeField]
    private GameObject inGameHUD; 
	// Use this for initialization
	void Awake () {
        connectionController = GetComponentInChildren<ConnectionController>();
        connectionController.Connected += ConnectionController_ConnectClicked;
	}
	
    void Start()
    {
        ShowConnectionUI(true);
    }

    void ConnectionController_ConnectClicked()
    {
        ShowConnectionUI(false);
    }

    private void ShowConnectionUI(bool show)
    {
		connectionController.gameObject.SetActive(show);
		inGameHUD.SetActive(!show);
    }

    void Update()
    {
        if(Input.GetKeyDown(KeyCode.Tab))
        {
            if (connectionController.gameObject.activeInHierarchy) ShowConnectionUI(false);
            else ShowConnectionUI(true);
        }
    }
}
