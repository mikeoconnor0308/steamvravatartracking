﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ConnectionController : MonoBehaviour
{
    public TMPro.TextMeshProUGUI hostText;
    public TMPro.TextMeshProUGUI hostDefaultText;

    public TMPro.TextMeshProUGUI connectionFailureText;

    public TMPro.TextMeshProUGUI portText;
    public TMPro.TextMeshProUGUI portDefaultText;

    private AvatarRenderConnection rendererConnection;
    private ConnectionStatusIndicator connectionStatusIndicator;

    public event System.Action Connected;

    private void Awake()
    {
        rendererConnection = FindObjectOfType<AvatarRenderConnection>();
        connectionStatusIndicator = FindObjectOfType<ConnectionStatusIndicator>();
    }

    /// <summary>
    /// Handles the connect button being clicked by starting the connection process.
    /// </summary>
    public void ConnectClicked()
    {

        string host = hostText.text.Trim();
        if (host == string.Empty || host == "" || host.Length < 2) host = hostDefaultText.text;
        string port = portText.text.Trim();
        if (port == string.Empty || port == "" || port.Length < 2) port = portDefaultText.text;

        int portNum;
        bool validInt = int.TryParse(port.Trim(), out portNum);
        if (validInt == false)
        {
			Debug.Log("Invalid int!");
            return;
        }

        
        bool connected = rendererConnection.ConnectTo(host, portNum);
        if (connected) 
        {
			connectionFailureText.gameObject.SetActive(false);
            connectionStatusIndicator.SetConnected();
            if (Connected != null) Connected.Invoke();
        }
        else
        {
            connectionStatusIndicator.SetDisconnected();
            connectionFailureText.gameObject.SetActive(true);
        }


    }
}