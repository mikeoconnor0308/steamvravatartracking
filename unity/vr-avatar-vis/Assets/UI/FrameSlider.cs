﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

/// <summary>
/// Slider for controlling frame being rendered.
/// </summary>
public class FrameSlider : MonoBehaviour {

    private AvatarFrameProcessor avatarFrameProcessor;
    private Slider slider;
    private TMPro.TextMeshProUGUI text;
    private void Start()
    {
        avatarFrameProcessor = FindObjectOfType<AvatarFrameProcessor>();
        slider = GetComponent<Slider>();
        slider.onValueChanged.AddListener(OnSliderValueChanged);
        text = GetComponentInChildren<TMPro.TextMeshProUGUI>();

    }

    private void OnSliderValueChanged(float val)
    {
        avatarFrameProcessor.SetFrame(val);
        text.text = ((int)val).ToString();
    }

    private void Update()
    {
        slider.minValue = 0;
        slider.maxValue = avatarFrameProcessor.Frames.Count;
        slider.value = avatarFrameProcessor.CurrentFrameIndex;
    }
}
