﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class PlaybackSpeedSlider : MonoBehaviour {

    private Slider slider;
    private TMPro.TextMeshProUGUI text;
    private AvatarFrameProcessor frameProcessor; 

	// Use this for initialization
	void Start () {
        slider = GetComponentInChildren<Slider>();
        text = GetComponentInChildren<TMPro.TextMeshProUGUI>();
        slider.onValueChanged.AddListener(OnValueChanged);
        frameProcessor = FindObjectOfType<AvatarFrameProcessor>();
	}

    private void OnValueChanged(float arg0)
    {
        frameProcessor.PlaybackFPS = arg0;
        text.text = $"FPS: {(int)arg0}";
    }

}
