﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

/// <summary>
/// Class for controlling rendering options.
/// </summary>
public class RenderingOptionsController : MonoBehaviour {

    [SerializeField]
    public AvatarRenderer TargetRenderer;

    [SerializeField]
    public AvatarRenderer PredictedRenderer;

	[SerializeField]
	private Toggle TargetRendererToggle;

	[SerializeField]
	private Toggle PredictedRendererToggle;

    [SerializeField]
    private Toggle TargetLinesToggle;

	[SerializeField]
	private Toggle PredictedLinesToggle;

    public void Start()
    {
        TargetRendererToggle.onValueChanged.AddListener(EnableTargetRenderer);
        PredictedRendererToggle.onValueChanged.AddListener(EnablePredictedRenderer);
        TargetLinesToggle.onValueChanged.AddListener(EnableTargetLines);
        PredictedLinesToggle.onValueChanged.AddListener(EnablePredictedLines);
    }

    public void EnableTargetRenderer(bool enable)
    {
        TargetRenderer.gameObject.SetActive(enable);
        TargetLinesToggle.interactable = enable;
    }

    public void EnablePredictedRenderer(bool enable)
    {
        PredictedRenderer.gameObject.SetActive(enable);
		PredictedLinesToggle.interactable = enable;        
    }

    public void EnableTargetLines(bool enable)
    {
        TargetRenderer.DrawLines = enable;
    }

    public void EnablePredictedLines(bool enable)
    {
        PredictedRenderer.DrawLines = enable; 
    }

}
