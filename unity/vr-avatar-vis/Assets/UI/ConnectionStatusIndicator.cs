﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Connection status indicator.
/// </summary>
public class ConnectionStatusIndicator : MonoBehaviour
{
    [SerializeField]
    private TMPro.TextMeshProUGUI text;

    [SerializeField]
    private Color connectingColor;

    [SerializeField]
    private Color connectedColor;

    [SerializeField]
    private Color disconnectedColor;

    private void Start()
    {
        AvatarRenderConnection connection = FindObjectOfType<AvatarRenderConnection>();
        connection.Connected += SetConnected;
        connection.Disconnected += SetDisconnected;

    }
    public void SetConnecting()
    {
        text.text = "Connecting...";
        text.color = connectingColor;
    }

    public void SetConnected()
    {
        text.text = "Connected!";
        text.color = connectedColor;
    }

    public void SetDisconnected()
    {
        text.text = "Disconnected...";
        text.color = disconnectedColor;
    }
}