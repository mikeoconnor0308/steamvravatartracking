﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlaybackControls : MonoBehaviour {

    public AvatarFrameProcessor frameProcessor;
	// Use this for initialization
	void Start () {
        frameProcessor = FindObjectOfType<AvatarFrameProcessor>();
	}
	
    public void SetPaused()
    {
        frameProcessor.SetPaused();
    }

    public void StepForward()
    {
        frameProcessor.StepForward();
    }

    public void StepBack()
    {
        frameProcessor.StepBack();
    }

    public void SetPlay()
    {
        frameProcessor.SetPaused(false);
    }
}
