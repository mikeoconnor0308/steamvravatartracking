﻿﻿using System.Collections;
using System.Collections.Generic;
using System.Net;
using System.Net.Sockets;
using System;
using System.IO;
using Newtonsoft.Json;

public class JsonClient
{
    private TcpClient tcpClient;

    /// <summary>
    /// Gets a value indicating whether this <see cref="T:JsonClient"/> is connected.
    /// </summary>
    /// <value><c>true</c> if connected; otherwise, <c>false</c>.</value>
    public bool Connected { get { return tcpClient != null && tcpClient.Connected == true; } }

    /// <summary>
    /// How long to poll for when testing if connected in microseconds.
    /// </summary>
	public int PollTime = 1000;

    private NetworkStream stream;
    private StreamReader rd;

    public void Dispose()
    {
        if (tcpClient != null)
        {
            tcpClient.Client.Close();
        }
        tcpClient = null;
        if (rd != null) rd.Dispose();
        if (stream != null) stream.Dispose();
    }

    internal void Connect(string host, int port)
    {
        if (tcpClient != null) Dispose();
        tcpClient = new TcpClient(host, port);
        tcpClient.Client.Blocking = true;
        stream = tcpClient.GetStream();
        rd = new StreamReader(stream);
    }

    private bool TestConnection()
    {
        if (Connected == false) return false;
        bool success = tcpClient.Client.Poll(PollTime, SelectMode.SelectRead);
        if (success == false)
        {
            Dispose();
            return false;
        }
        else return true;
    }
    internal bool ReceiveJsonTransforms(out Dictionary<string, List<float>> dictionaryReceived)
    {
        dictionaryReceived = new Dictionary<string, List<float>>();
        if (Connected == false) return false;
        try
        {
            //Test connection before attempting to receive data.
            bool connected = TestConnection();
            if (!connected) return false;
            if (tcpClient.Client.Available > 0)
            {
                string jsonReceived = rd.ReadLine();
                dictionaryReceived = JsonConvert.DeserializeObject<Dictionary<string, List<float>>>(jsonReceived);
            }
            else return false;
        }
        catch (Exception e)
        {
            throw new Exception("Exception thrown attempting to receive json data", e);
        }

        return true;
    }
}