﻿﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

/// <summary>
/// Class for managing a connection for avatar rendering.
/// </summary>
public class AvatarRenderConnection : MonoBehaviour
{

    /// <summary>
    /// Gets a value indicating whether this <see cref="T:AvatarRenderConnection"/> is connected.
    /// </summary>
    /// <value><c>true</c> if connected; otherwise, <c>false</c>.</value>
    public bool IsConnected { get { return client != null && client.Connected == true; } }

    /// <summary>
    /// Occurs when data received.
    /// </summary>
    public event EventHandler DataReceived;

    /// <summary>
    /// Occurs when client connects.
    /// </summary>
    public event Action Connected;

    /// <summary>
    /// Occurs when client disconnects.
    /// </summary>
    public event Action Disconnected;

    private JsonClient client;

    private string host;
    private int port;
    private bool hadConnection;

    /// <summary>
    /// Attempts to connect the specified host and port.
    /// </summary>
    /// <returns><c>true</c>, if connected, <c>false</c> otherwise.</returns>
    /// <param name="host">Host.</param>
    /// <param name="port">Port.</param>
    /// <remarks>
    /// Upon an unsuccessful connection, it will keep attempting to connect.
    /// </remarks>
    public bool ConnectTo(string host, int port)
    {
        if (client != null) client.Dispose();
        client = new JsonClient();
        this.host = host;
        this.port = port;
        //Return whether we succeed to connect or not.
        bool success =  TryToConnect();
        if (success && Connected != null) Connected();
        return success;
    }

    /// <summary>
    /// Attempts to connect to the current client.
    /// </summary>
    /// <returns><c>true</c>, if successful connection, <c>false</c> otherwise.</returns>
    private bool TryToConnect()
    {
        if (client == null) return false;
        try
        {
            client?.Connect(host, port);
        }
        catch (Exception e)
        {
            Debug.Log("Exception thrown attempting to connect json, will keep trying to connect..");
            Debug.LogException(e, this);
            return false;
        }
        hadConnection = true;
        return true;
    }

    /// <summary>
    /// Called every frame, if connected it will process any new data.
    /// </summary>
    private void Update()
    {
        if (!IsConnected)
        {
            //If we did have a connection last frame, then fire off an event to let 
            //anything that is listening know that we're disconnected.
            if(hadConnection)
            {
                if (Disconnected != null) Disconnected();
                hadConnection = false;
            }
            return;
        }

        Dictionary<string, List<float>> receivedTransforms = new Dictionary<string, List<float>>();
        bool newData = false;
        try
        {
            newData = client.ReceiveJsonTransforms(out receivedTransforms);
        }
        catch (System.Net.Sockets.SocketException exception)
        {
            Debug.Log("Exception thrown attempting to receive json, will try to reconnect..");
            Debug.LogException(exception, this);
        }
        if (newData)
        {
            if (DataReceived != null) DataReceived(receivedTransforms, null);
        }
    }

}