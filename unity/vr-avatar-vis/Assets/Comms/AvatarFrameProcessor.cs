using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

/// <summary>
/// Class for managing the frames for rendering avatar. 
/// </summary>
public class AvatarFrameProcessor : MonoBehaviour
{
	/// <summary>
	/// The labels used to process data sent over the socket.
	/// </summary>
	/// <remarks>
	/// We have to assume that the socket is sending labels that match these.
	/// </remarks>
	public string[] KnownLabels = new[]
		{"Headset", "LController", "RController", "LeftElbow", "RightElbow", "Front", "Back", "LeftKnee", "RightKnee"};


    /// <summary>
    /// Modifier to add to each label in the predicted avatar.
    /// </summary>
    public static string PredictionModifier = "Pred";

	/// <summary>
	/// Transform of the headset.
	/// </summary>
	public Transform Headset;

	/// <summary>
	/// Transform of the left controller.
	/// </summary>
	public Transform LController;

	/// <summary>
	/// Transform of the right controller.
	/// </summary>
	public Transform RController;

	/// <summary>
	/// The renderer for the target avatar.
	/// </summary>
	public AvatarRenderer TargetAvatar;

	/// <summary>
	/// The renderer for the predicted avatar.
	/// </summary>
	public AvatarRenderer PredictedAvatar;

    /// <summary>
    /// The playback fps.
    /// </summary>
	public float PlaybackFPS = 10;

    /// <summary>
    /// Whether the player will loop or not.
    /// </summary>
	public bool Looping = true;

	/// <summary>
	/// The set of frames received.
	/// </summary>
	public List<AvatarFrame> Frames = new List<AvatarFrame>();

	/// <summary>
	/// Occurs when frames changed. 
	/// </summary>
	public event EventHandler FramesChanged;

	/// <summary>
	/// Gets the current frame.
	/// </summary>
	/// <value>The current frame.</value>
	public AvatarFrame CurrentFrame
	{
		get
		{
			if (CurrentFrameIndex > 0 && CurrentFrameIndex < Frames.Count)
			{
				return Frames[(int)CurrentFrameIndex];
			}
			else return null;
		}
	}

	public float CurrentFrameIndex;

	/// <summary>
	/// Stores a map from the labels that we'll receive in the json to transforms that will be rendered.
	/// </summary>
	private Dictionary<string, Transform> jsonToTransform = new Dictionary<string, Transform>();
    private bool Paused;

    private void Start()
	{
		GenerateLabelsToTransformMap();
        AvatarRenderConnection connection = GetComponent<AvatarRenderConnection>();
        connection.DataReceived += ProcessReceivedData;
	}

	private void GenerateLabelsToTransformMap()
	{
		var targetTransforms = TargetAvatar.GetComponentsInChildren<Transform>();
		var predictedTransforms = PredictedAvatar.GetComponentsInChildren<Transform>();
		//Have to assume these won't change.
		jsonToTransform["Headset"] = Headset;
		jsonToTransform["LController"] = LController;
		jsonToTransform["RController"] = RController;
		//Loop over all the known labels and point to the object in the heirarchy that has the same name. Bit of a hack but it'll work!
		foreach (string label in KnownLabels)
		{
			foreach (Transform targetTransform in targetTransforms)
			{
				if (targetTransform.gameObject.name == label) jsonToTransform[label] = targetTransform;
			}
			foreach (Transform predTransform in predictedTransforms)
			{
				if (predTransform.gameObject.name == label) jsonToTransform[label + PredictionModifier] = predTransform;
			}
		}
	}

	// Update is called once per frame
	void Update()
    {
        if (Frames.Count > 0)
        {
            if(!Paused)
            {
				CurrentFrameIndex += Time.deltaTime * PlaybackFPS;

				if (Looping)
				{
					CurrentFrameIndex = CurrentFrameIndex % (Frames.Count);
				}
				else
				{
					CurrentFrameIndex = Mathf.Min(CurrentFrameIndex, Frames.Count - 1);
				}              
            }
            CurrentFrameIndex = Mathf.Clamp(CurrentFrameIndex, 0f, Frames.Count - 1);
            RenderFrame(Frames[(int)CurrentFrameIndex]);
        }

	}


	private void ProcessReceivedData(object sender, EventArgs e)
	{
        Dictionary<string, List<float>> jsonReceived = (Dictionary<string, List<float>>) sender;
		AvatarFrame newFrame = new AvatarFrame(jsonReceived);
        AddFrame(newFrame);
	}

    /// <summary>
    /// Adds the frame.
    /// </summary>
    /// <param name="frame">Frame.</param>
    public void AddFrame(AvatarFrame frame)
    {
        Frames.Add(frame);
        if (FramesChanged != null) FramesChanged(Frames, null);
    }

    /// <summary>
    /// Reset this instance.
    /// </summary>
    public void Reset()
    {
        Frames.Clear();
        CurrentFrameIndex = 0;
    }

    /// <summary>
    /// Sets the frame.
    /// </summary>
    /// <param name="frameIndex">Frame index.</param>
    public void SetFrame(float frameIndex)
    {
        if (frameIndex > 0 && frameIndex < Frames.Count)
        {
            CurrentFrameIndex = frameIndex;
        }
    }

	/// <summary>
	/// Renders the frame, by moving all the transforms to locations specified in the frame.
	/// </summary>
	/// <param name="frame">Frame to render.</param>
	public void RenderFrame(AvatarFrame frame)
	{
		var frameTransforms = frame.Transforms;
		foreach (var keyVal in frameTransforms)
		{
			var label = keyVal.Key;
			if (jsonToTransform.ContainsKey(label) == false)
			{
				Debug.LogWarning($"Unknown label received: {keyVal.Key}");
				continue;
			}

			//Use the dictionary to get the transform to set the position of
			jsonToTransform[label].position = new Vector3(keyVal.Value[0], keyVal.Value[1], keyVal.Value[2]);
			//Todo add support for quaternions...
		}
	}

    /// <summary>
    /// Steps playback back a frame and pauses.
    /// </summary>
	public void StepBack()
	{
		SetPaused();
        CurrentFrameIndex--;
	}

    /// <summary>
    /// Steps playback to paused.
    /// </summary>
    /// <param name="v">If set to <c>true</c> v.</param>
	public void SetPaused(bool v = true)
	{
		Paused = v;
	}

    /// <summary>
    /// Steps playback forward a frame.
    /// </summary>
	public void StepForward()
    {
        SetPaused();
        CurrentFrameIndex++;
    }
}
