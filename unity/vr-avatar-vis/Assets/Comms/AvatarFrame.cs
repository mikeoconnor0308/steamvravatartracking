using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class AvatarFrame
{
    public Dictionary<string, List<float>> Transforms; 

    public AvatarFrame(Dictionary<string, List<float>> transforms)
    {
        Transforms = transforms;
    }

}
