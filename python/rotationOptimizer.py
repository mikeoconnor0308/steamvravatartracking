import math
import numpy as np


def Calculate3PointRotationMatrix(MWQdyn,MWQref,nat,Lin):

    # find the normal to the plane of the MWQref geometry
    if Lin:
        vnorm1=[1.0,0.0,0.0]
        print "\nWarning: there is no molecular plane for a 3-atom linear molecule"
        print "using", vnorm1, "as the normal vector...\n"
    else:
        #   find the normal to the plane of the MWQref geometry
        v1=MWQref[0]-MWQref[1]
        v2=MWQref[2]-MWQref[1]
        vnorm1=VecNorm(np.cross(v1,v2))

    # find the normal to the plane of the MWQdyn geometry
    v1=MWQdyn[0]-MWQdyn[1]
    v2=MWQdyn[2]-MWQdyn[1]
    vnorm2=VecNorm(np.cross(v1,v2))

    # find a unit vector along the seam of intersection between planes
    vintersect=VecNorm(np.cross(vnorm1,vnorm2))

    # find the angle for rotation about this unit vector & rotate
    a=math.acos(np.dot(vnorm1,vnorm2))
    R1=RotationMatrix(vintersect,a)
    MWQdynI=(np.matrix(MWQdyn))*(np.matrix(R1))

    # find the second rotation required to minimize the least squares
    C=0.0
    S=0.0

    for i in range(0,3):
        C=C+np.dot(MWQdynI[i],MWQref[i])
        S=S+np.dot(np.cross(MWQdynI[i],MWQref[i]),vnorm1)

    a=math.sqrt(S**2+C**2)

    # the rotation angle about vnorm 1 may be either theta1 or theta2
    # check which gives the minimum least squares & choose that one
    theta1=-1.0*math.atan(S/C)
    theta2=theta1-math.pi

    R2theta1=RotationMatrix(vnorm1,theta1)
    GeomTheta1=np.array(np.matrix(MWQdynI)*np.matrix(R2theta1))

    R2theta2=RotationMatrix(vnorm1,theta2)
    GeomTheta2=np.array(np.matrix(MWQdynI)*np.matrix(R2theta2))

    LS1=0.0
    LS2=0.0
    for i in range(0,nat):
        for j in range(0,3):
            LS1 += (GeomTheta1[i][j]-MWQref[i][j])**2
            LS2 += (GeomTheta2[i][j]-MWQref[i][j])**2
    if(LS1<LS2):
        R2=R2theta1
    else:
        R2=R2theta2

    # calculate the final rotation matrix as the product of R1 and R2
    Rmat=np.array(np.matrix(R1)*np.matrix(R2))

    return Rmat


def VecNorm(vec):
    """ function to normalize a vector """

    n=np.linalg.norm(vec)
    normvec=[]
    for i in range(0,len(vec)):
        normvec.append(vec[i]/n)

    return normvec


def RotationMatrix(axis,angle):
    """ function to construct a rotation matrix from an axis & angle (rad) """

    rotm=np.zeros((3,3))

    ux=axis[0]
    uy=axis[1]
    uz=axis[2]
    sinth=math.sin(angle)
    costh=math.cos(angle)
    costhm=1.0-costh

    # Form the components of the rotation matrix.

    rotm[0][0] = costhm * ux * ux + costh
    rotm[0][1] = costhm * ux * uy - sinth * uz
    rotm[0][2] = costhm * ux * uz + sinth * uy
    rotm[1][0] = costhm * uy * ux + sinth * uz
    rotm[1][1] = costhm * uy * uy + costh
    rotm[1][2] = costhm * uy * uz - sinth * ux
    rotm[2][0] = costhm * uz * ux - sinth * uy
    rotm[2][1] = costhm * uz * uy + sinth * ux
    rotm[2][2] = costhm * uz * uz + costh

    return rotm

if __name__ == "__main__":

    # first let's generate the body points in the reference frame
    nbodyPts = 6
    bodyPts = np.zeros((nbodyPts, 3))
    bodyPts[0] = [ 0.0, 5.0, 0.0]
    bodyPts[1] = [-3.0, 4.0, 0.0]
    bodyPts[2] = [ 3.0, 4.0, 0.0]
    bodyPts[3] = [ 0.0, 3.0, 0.0]
    bodyPts[4] = [-1.0, 2.0, 0.0]
    bodyPts[5] = [ 1.0, 2.0, 0.0]

    # now let's generate our reference points (head & hands)
    nheadHandsRefPts = 3
    headHandsRefPts = np.zeros((nheadHandsRefPts, 3))
    headHandsRefPts[0] = bodyPts[0]
    headHandsRefPts[1] = bodyPts[1]
    headHandsRefPts[2] = bodyPts[2]

    print headHandsRefPts

    # now let's generate a random rotation matrix about the y axis (pretend we don't know what theta is)
    rotationAxis = [0,1,0]
    R1 = RotationMatrix(rotationAxis,math.pi/2.0)
    # rotate the body coordinates by theta
    rotatedBodyPts=np.array(np.matrix(bodyPts)*np.matrix(R1))

    print rotatedBodyPts

    # now get rotated head & hands
    headHandsRotated = np.zeros((nbodyPts, 3))
    headHandsRotated[0] = rotatedBodyPts[0]
    headHandsRotated[1] = rotatedBodyPts[1]
    headHandsRotated[2] = rotatedBodyPts[2]

    # now, based on head & hands, we need to figure out the rotation matrix that will best align Qrot with the headHandsRefPts

    Linear = False
    R2 = Calculate3PointRotationMatrix(headHandsRotated, headHandsRefPts, nheadHandsRefPts, Linear)

    headHandsRefPts = np.zeros((nheadHandsRefPts, 3))
    headHandsRefPts[0] = bodyPts[0]
    headHandsRefPts[1] = bodyPts[1]
    headHandsRefPts[2] = bodyPts[2]

    # print R2

    QrotatedBack=np.array(np.matrix(rotatedBodyPts)*np.matrix(R2))

    print QrotatedBack

    print "that's all folks"

