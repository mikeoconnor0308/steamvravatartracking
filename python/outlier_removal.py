# http://scikit-learn.org/stable/auto_examples/covariance/plot_outlier_detection.html#example-covariance-plot-outlier-detection-py
from sklearn.ensemble import IsolationForest
import matplotlib.pyplot as plt
import numpy as np


# function finds outliers and return array of booleans, true if not outlier
def find_outliers(data,outliers_fraction):
    rng = np.random.RandomState(42)
    n_samples = 256

    clf = IsolationForest(max_samples=n_samples,contamination=outliers_fraction,random_state=rng)

    clf.fit(data)
    y_pred = clf.predict(data)
    outlier_mapping = np.array([x == +1 for x in y_pred])  # Create boolean array of inlier/outlier classifications
    return outlier_mapping

# removes outliers from targets and features according to given fractions
#  returns pruned targets and features arrays, plots pruned and unpruned distributions
def remove_outliers(Targets, Features, Targ_frac, Feat_frac):
    outlier_mapping_features = find_outliers(Features, Feat_frac)  # Array of inlier classifications in features
    outlier_mapping_targets = find_outliers(Targets, Targ_frac)  # Array of inlier classifications in targets

    outlier_mapping_all = np.array([(x and y) for x, y in zip(outlier_mapping_features, outlier_mapping_targets)])
    num_outliers = outlier_mapping_all.sum()  # Total number of inliers
    print num_outliers
    Features_pruned = Features[outlier_mapping_all]
    Targets_pruned = Targets[outlier_mapping_all]
    plt.clf()
    fig, axes = plt.subplots(Targets_pruned.shape[1], 1)

    fig.set_size_inches(6, 4*Targets_pruned.shape[1])
    for i in range(len(axes)):
        binwidth = 0.1
        bins = np.arange(min(Targets[:, i]), max(Targets[:, i]) + binwidth, binwidth)
        axes[i].hist(Targets[:, i], bins=bins, stacked=False)
        axes[i].hist(Targets_pruned[:, i], bins=bins, stacked=False)

    plt.show()
    return Targets_pruned, Features_pruned