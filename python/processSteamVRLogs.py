import glob
import os
import json
import pandas as pd
import sys
from pprint import pprint


def process_label(player_id, label_left, label_right, mapping_left):
    left_controller_str = "LController{0}".format(player_id)
    right_controller_str = "RController{0}".format(player_id)
    coords = ["X", "Y", "Z"]
    for coord in coords:
        left_str = label_left + coord
        back_str = label_right + coord
        if mapping[mapping_left] == "right_controller":
            left_controller = right_controller_str + coord
            right_controller = left_controller_str + coord
        else:
            left_controller = left_controller_str + coord
            right_controller = right_controller_str + coord
        outputDF[left_str] = data[left_controller].copy()
        outputDF[back_str] = data[right_controller].copy()


def process_player(player_id):
    player_str = "Player{0}ID".format(player_id)
    player_ip = data.at[1, player_str]
    if player_ip == mapping["front_back_ip"]:
        process_label(player_id, "Front", "Back", "front")
    elif player_ip == mapping["elbows_ip"]:
        process_label(player_id, "LeftElbow", "RightElbow", "left_elbow")
    elif player_ip == mapping["knees_ip"]:
        process_label(player_id, "LeftKnee", "RightKnee", "left_knee")

logPath = "../steamVRLogs/*.csv"
json_ext = "_notes.json"
outputDirectory = "../data"

if not os.path.exists(outputDirectory):
    os.makedirs(outputDirectory)

# loop over all the csv files.
file_id = 0
for csvFileName in glob.glob(logPath):

    # Read in the matching json file.
    jsonFile = "{0}{1}".format(os.path.splitext(csvFileName)[0], json_ext)
    with open(jsonFile) as noteFile:
        mapping = json.load(noteFile)

    #
    data = pd.read_csv(csvFileName)
    outputDF = pd.DataFrame()

    # get the local player columns, which are immediately useable.
    nLocalPlayerCols = 23
    outputDF = data.ix[:,0:nLocalPlayerCols].copy()

    for playerID in range(2, 5):
        process_player(playerID)

    scale = "1"
    if "scale" in mapping:
        scale = mapping["scale"]
    outputDF["Scale"] = scale
    print(outputDF.head())
    outputDF.to_csv(os.path.join(outputDirectory, "steamvr_avatar_{0}".format(file_id)), index=False)
    file_id += 1
