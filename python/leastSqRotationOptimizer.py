import numpy as np
import rotationOptimizer
import math
from scipy.optimize import leastsq

def generateChiSqdForSomeRotationMatrix(parameters,referencePts,PtsToRotate):
    theta = parameters[0]
    rotationAxis = [0, 1, 0]
    rotationMatrix = rotationOptimizer.RotationMatrix(rotationAxis, theta)
    rotatedPts = np.array(np.matrix(PtsToRotate) * np.matrix(rotationMatrix))

    ref = referencePts.flatten()
    rot = rotatedPts.flatten()

    chisqd = 0.0
    chisqd = (np.power(ref - rot, 2))

    return chisqd


# first let's generate the body points in the reference frame
nbodyPts = 6
bodyPts = np.zeros((nbodyPts, 3))
bodyPts[0] = [ 0.0, 5.0, 0.0]
bodyPts[1] = [-3.0, 4.0, 0.0]
bodyPts[2] = [ 3.0, 4.0, 0.0]
bodyPts[3] = [ 0.0, 3.0, 0.0]
bodyPts[4] = [-1.0, 2.0, 0.0]
bodyPts[5] = [ 1.0, 2.0, 0.0]

print "bodyPts"
print bodyPts

# now let's generate our reference points (head & hands)
nheadHandsRefPts = 3
headHandsRefPts = np.zeros((nheadHandsRefPts, 3))
headHandsRefPts[0] = bodyPts[0]
headHandsRefPts[1] = bodyPts[1]
headHandsRefPts[2] = bodyPts[2]

print "headHandsRefPts"
print headHandsRefPts

# now let's generate a random rotation matrix about the y axis (pretend we don't know what theta is)
testTheta = math.pi/2.0
rotationAxis = [0,1,0]
R1 = rotationOptimizer.RotationMatrix(rotationAxis,testTheta)
# rotate the body coordinates by theta
rotatedBodyPts=np.array(np.matrix(bodyPts)*np.matrix(R1))

print "BodyPts rotated by", testTheta
print rotatedBodyPts

# now get rotated head & hands
headHandsRotated = np.zeros((3, 3))
headHandsRotated[0] = rotatedBodyPts[0]
headHandsRotated[1] = rotatedBodyPts[1]
headHandsRotated[2] = rotatedBodyPts[2]

# now, based on head & hands, we need to figure out the rotation matrix that will best align Qrot with the headHandsRefPts

theta = math.pi/3
# chisqd = generateChiSqdForSomeRotationMatrix(theta, headHandsRefPts, headHandsRotated)

parameters = [0.0] * 1
parameters[0] = theta  # parameters[0] is mu [centre of the distribution]
print "initial guess theta", theta
finalParameters = leastsq(generateChiSqdForSomeRotationMatrix, parameters, args=(headHandsRefPts, headHandsRotated))
leastSqOptimizedTheta = finalParameters[0][0]

print "leastSqOptimizedTheta", leastSqOptimizedTheta

headHandsRefPts = np.zeros((nheadHandsRefPts, 3))
headHandsRefPts[0] = bodyPts[0]
headHandsRefPts[1] = bodyPts[1]
headHandsRefPts[2] = bodyPts[2]

# print R2
rotationAxis = [0, 1, 0]
R2 = rotationOptimizer.RotationMatrix(rotationAxis, leastSqOptimizedTheta)
bodyPtsRotatedBack=np.array(np.matrix(rotatedBodyPts)*np.matrix(R2))

print "bodyPtsRotatedBack"
print bodyPtsRotatedBack

print "that's all folks"

