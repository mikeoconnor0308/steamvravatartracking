import pandas as pd
import glob
import subprocess as sp
import numpy as np
import rotationOptimizer as ro
from scipy.optimize import leastsq as opt_ls


def rotate(angle, X, Z):
    Xout = np.cos(angle) * X - np.sin(angle) * Z
    Zout = np.sin(angle) * X + np.cos(angle) * Z
    print(Xout.shape)
    return Xout, Zout

def rotate_find(angle, X, Z, X1, Z1):
    Xout = np.cos(angle) * X - np.sin(angle) * Z
    Zout = np.sin(angle) * X + np.cos(angle) * Z
    #print(Xout.shape)
    return np.hstack((Xout-X1, Zout-Z1))

def generate_labels_for_target(target_label):
    coords = ['X', 'Y', 'Z']
    return ["{0}{1}".format(target_label, coord) for coord in coords]


def read_data(path,k=-1, target_labels=['Front']):
    num_targets = len(target_labels)
    datafiles =  glob.glob(path+'data/*')
    print(datafiles)
    if k>-1:
        df = pd.read_csv(datafiles[k])
    else:
        framelist = []
        for file in datafiles:
            framelist.append(pd.read_csv(file))
        df = pd.concat(framelist)
    df = df.dropna()

    Features = df[['LControllerX','LControllerY','LControllerZ',
                   'RControllerX','RControllerY','RControllerZ',
                   'HeadsetX','HeadsetY','HeadsetZ']].values

    listOfTargets = [generate_labels_for_target(targetLabel) for targetLabel in target_labels]
    flatten = lambda l: [item for sublist in l for item in sublist]
    targetFields = flatten(listOfTargets)
    Targets = df[targetFields].values

    Scale = df['Scale'].values
    Scale = Scale.reshape((Scale.size,1))

    Features *= Scale
    Targets *= Scale

    VScale = Features[0,7]

    Features /= VScale
    Targets /= VScale


    AveX = (Features[:,0]+Features[:,3]+Features[:,6]).reshape((Features.shape[0],1))/3
    AveZ = (Features[:,2]+Features[:,5]+Features[:,8]).reshape((Features.shape[0],1))/3

    Translation = np.hstack((AveX,np.zeros(AveX.shape),AveZ))
    Feature_Translation = np.hstack([Translation] * 3)
    TargetTranslation = np.hstack([Translation] * num_targets)
    Features -= Feature_Translation
    Targets -= TargetTranslation


    Reference = np.array([[1.,0,0],[-1,0,0],[0,1,0]])
    #Reference =  Features[0].reshape((3,3))
    #Reference = np.array([[5., 1., 0.001], [-5, 1., 0.001], [0, 1., 0.001]])
    #print(Reference)
    #print("AAA")
    """
    for i in range(0,Features.shape[0]):
        Current_F = Features[i].reshape((3,3))
        Current_T = Targets[i].reshape((num_targets,3))
        #print(type(Current))
        Matrix = ro.Calculate3PointRotationMatrix(Current_F,Reference,3,False)
        #print(type(Matrix))
        Aligned_Features = np.dot(Current_F, Matrix)
        Aligned_Targets = np.dot(Current_T, Matrix)
        Features[i] = Aligned_Features.reshape((9))
        Targets[i] = Aligned_Targets.reshape((3*num_targets))
    """
    Reference_X = np.array([-1,1,0])
    Reference_Z = np.array([0,0,0])
    for i in range(Features.shape[0]):
        Current_F = Features[i].reshape((3, 3))
        Current_T = Targets[i].reshape((num_targets, 3))
        Features_X = Current_F[:,0]
        Features_Z = Current_F[:,2]
        angle_found = opt_ls(rotate_find,0,args=(Features_X,Features_Z,Reference_X,Reference_Z))[0][0]
        New_FX, New_FZ = rotate(angle_found,Features_X,Features_Z)
        Current_F[:,0] = New_FX
        Current_F[:,2] = New_FZ
        Targets_X = Current_T[:, 0]
        Targets_Z = Current_T[:, 2]
        New_TX, New_TZ = rotate(angle_found, Targets_X, Targets_Z)
        Current_T[:, 0] = New_TX
        Current_T[:, 2] = New_TZ

        Features[i] = Current_F.reshape((9))
        Targets[i] = Current_T.reshape((3 * num_targets))

    return Features, Targets


def get_frame_slice(slice, frames):
    """
    Return frame slice reshaped in a format compatible with rendering. 
    :type frames: numpy array.
    :param s: A numpy slice object (or index)
    :param frames: dataframe object.
    :return: Slice of frames array. 
    """
    # number of targets is 1/3 of dimensionality
    n_targets = frames.shape[1] / 3
    df = frames.reshape((frames.shape[0], n_targets, 3))  # Number of samples, Number of features, XYZ
    return df[slice]

if __name__ == "__main__":
    print(get_frame_slice(0,read_data('',1, ['Front', 'Back'])[0]))
    print(get_frame_slice(1,read_data('',1, ['Front', 'Back'])[0]))
    print(get_frame_slice(1, read_data('', 1, ['Front', 'Back'])[1]))
    #def features():
    #print read_data('',-1)
